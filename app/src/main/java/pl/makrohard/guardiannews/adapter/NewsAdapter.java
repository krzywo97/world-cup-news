package pl.makrohard.guardiannews.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import pl.makrohard.guardiannews.R;
import pl.makrohard.guardiannews.model.News;

public class NewsAdapter extends BaseAdapter {
    private List<News> news;
    private Context context;

    public NewsAdapter(Context context, List<News> news) {
        this.news = news;
        this.context = context;
    }

    public void clear() {
        news.clear();
        notifyDataSetChanged();
    }

    public void addAll(List<News> news) {
        this.news.addAll(news);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return news.size();
    }

    @Override
    public News getItem(int position) {
        return news.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.feed_item, parent, false);
            viewHolder = new ViewHolder(convertView.findViewById(R.id.feed_item_container),
                    (TextView) convertView.findViewById(R.id.feed_item_title),
                    (TextView) convertView.findViewById(R.id.feed_item_section),
                    (TextView) convertView.findViewById(R.id.feed_item_author),
                    (TextView) convertView.findViewById(R.id.feed_item_date));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final News item = news.get(position);
        viewHolder.title.setText(item.getTitle());
        viewHolder.section.setText(item.getSection());
        viewHolder.author.setText(item.getAuthor());
        viewHolder.date.setText(item.getPublishedDate());
        viewHolder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent view = new Intent();
                view.setAction(Intent.ACTION_VIEW);
                view.setData(Uri.parse(item.getUrl()));
                context.startActivity(view);
            }
        });
        return convertView;
    }

    private static class ViewHolder {
        private View container;
        private TextView title, section, author, date;

        private ViewHolder(View container, TextView title, TextView section, TextView author, TextView date) {
            this.container = container;
            this.title = title;
            this.section = section;
            this.author = author;
            this.date = date;
        }
    }
}
