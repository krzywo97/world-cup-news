package pl.makrohard.guardiannews.activity;

import android.app.LoaderManager;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pl.makrohard.guardiannews.R;
import pl.makrohard.guardiannews.adapter.NewsAdapter;
import pl.makrohard.guardiannews.model.News;
import pl.makrohard.guardiannews.util.NewsLoader;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<News>> {
    private static final int NEWS_LOADER = 1;

    private NewsAdapter newsAdapter;
    private TextView status;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView feed = findViewById(R.id.feed_list_view);
        status = findViewById(R.id.status);
        progressBar = findViewById(R.id.progress_bar);
        newsAdapter = new NewsAdapter(this, new ArrayList<News>());
        feed.setAdapter(newsAdapter);
        feed.setEmptyView(status);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.news_about_world_cup));
        }
        load();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                load();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<List<News>> onCreateLoader(int id, Bundle args) {
        return new NewsLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<List<News>> loader, List<News> data) {
        progressBar.setVisibility(View.INVISIBLE);
        newsAdapter.clear();
        if (data == null || data.size() == 0) {
            status.setText(getString(R.string.no_news));
            return;
        }
        newsAdapter.addAll(data);
    }

    @Override
    public void onLoaderReset(Loader<List<News>> loader) {
        newsAdapter.clear();
    }

    private void load() {
        if (!isNetworkAvailable()) {
            if (newsAdapter.getCount() > 0) {
                Toast.makeText(this, getString(R.string.no_connection), Toast.LENGTH_LONG).show();
            } else {
                status.setText(getString(R.string.no_connection));
            }
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        status.setText(null);
        getLoaderManager().initLoader(NEWS_LOADER, null, this).forceLoad();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (connectivityManager == null) return false;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isAvailable();
    }
}