package pl.makrohard.guardiannews.util;

import android.annotation.SuppressLint;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.makrohard.guardiannews.R;
import pl.makrohard.guardiannews.model.News;

public class NewsLoader extends AsyncTaskLoader<List<News>> {
    private static final String BASE_URL = "https://content.guardianapis.com/search";
    private static final String API_KEY = "22b84b7d-5c96-4283-acef-15848728f887";

    public NewsLoader(Context context) {
        super(context);
    }

    @Override
    public List<News> loadInBackground() {
        try {
            HttpURLConnection connection = makeConnection();
            if (connection.getResponseCode() != 200) {
                return new ArrayList<>();
            }
            String json = getResponse(connection.getInputStream());
            return parseJson(json);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private HttpURLConnection makeConnection() throws IOException {
        Uri uri = Uri.parse(BASE_URL);
        Uri.Builder builder = uri.buildUpon();
        builder.appendQueryParameter("api-key", API_KEY);
        builder.appendQueryParameter("q", "World Cup");
        builder.appendQueryParameter("show-tags", "contributor");
        builder.appendQueryParameter("page-size", "50");
        HttpURLConnection connection = (HttpURLConnection) new URL(builder.toString()).openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(10000);
        connection.setReadTimeout(15000);
        connection.connect();
        return connection;
    }

    private String getResponse(InputStream responseStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(responseStream));
        String line;
        StringBuilder response = new StringBuilder();
        while ((line = bufferedReader.readLine()) != null) {
            response.append(line);
        }
        return response.toString();
    }

    private List<News> parseJson(String json) throws JSONException {
        JSONObject root = new JSONObject(json);
        JSONObject response = root.getJSONObject("response");
        JSONArray results = response.getJSONArray("results");
        List<News> news = new ArrayList<>();
        for (int i = 0; i < results.length(); i++) {
            JSONObject result = results.getJSONObject(i);
            String title = result.getString("webTitle");
            String section = result.getString("sectionName");
            StringBuilder authors = new StringBuilder();
            JSONArray tags = result.getJSONArray("tags");
            for (int j = 0; j < tags.length(); j++) {
                JSONObject tag = tags.getJSONObject(j);
                String type = tag.getString("type");
                if (type.equals("contributor")) {
                    authors.append(tag.getString("webTitle"));
                    if (j < tags.length() - 1) authors.append(", ");
                }
            }
            String publishedDate = result.getString("webPublicationDate");
            String url = result.getString("webUrl");
            news.add(new News(title, section, authors.toString(), parseDate(publishedDate), url));
        }
        return news;
    }

    @SuppressLint("SimpleDateFormat")
    private String parseDate(String responseDate) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Date date = format.parse(responseDate);
            return date.toString();
        } catch (ParseException e) {
            e.printStackTrace();
            return getContext().getString(R.string.unknown_date);
        }
    }
}
