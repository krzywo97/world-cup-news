package pl.makrohard.guardiannews.model;

public class News {
    private String title, section, author, publishedDate, url;

    public News(String title, String section, String author, String publishedDate, String url) {
        this.title = title;
        this.section = section;
        this.author = author;
        this.publishedDate = publishedDate;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public String getSection() {
        return section;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public String getUrl() {
        return url;
    }
}
